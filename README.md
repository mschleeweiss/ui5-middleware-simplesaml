# ui5-middleware-simplesaml

A proxy that solves SAML auth in a very **non-elegant but simple** way.

Basically it uses [saml-solver](https://www.npmjs.com/package/saml-solver). This in turn opens Chromium, where the SAML request is waved through. The tokens are then picked up and used for the UI5 app.

**It works** most of the time, which was good enough for us.

## Usage

Add the following lines to your `ui5.yaml`. Some of these lines are optional

```yaml
- name: ui5-middleware-simplesaml
    beforeMiddleware: compression
    mountPath: /sap/opu/odata/
    configuration:
        baseUrl: "https://gwaas-xxxxxxxxxx.hana.ondemand.com"
        initPath: "/odata/sap/zping"
        manualInteractions:
            "https://adfs.startup.com/adfs/ls/": "#csspathofclickableelement"
        pathMappings:
            "/": "/odata/"
        debug: false
```


Also it should be SSO of course but sometimes it only works if your credentials are stored in some secret file on your machine:

Store this in `/$HOME/.saml_auth.json` (`/$HOME/saml_auth.json` on Windows)

```json
{ 
    "user": "myuser", 
    "password": "mypw"
}
```


## Settings

|Param|Description|
|-|-|
|`mountPath`|The relative path which should trigger proxying|
|`configuration.baseUrl`|The host where your requests should be proxied to|
|`configuration.initPath`| We found that the package only works if there is some kind of ping call before the actual requests from the app are started. This can be the same as one of your app's OData services.|
|`configuration.manualInteractions`|Not relevant for most use-cases but feel free to ask|


## Credit

[tobias_kay](https://gitlab.com/tobias_kay) did the heavy lifting so feel free to give him some GitLab stars!